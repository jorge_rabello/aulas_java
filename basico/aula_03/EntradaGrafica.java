package basico.aula_03;

import javax.swing.JOptionPane;

public class EntradaGrafica {

    public static void main(String[] args) {

        String nome = JOptionPane.showInputDialog(null, "Qual o seu nome ?");

        JOptionPane.showMessageDialog(null, "Seja bem-vindo " + nome, "Boas Vindas", JOptionPane.INFORMATION_MESSAGE);

    }

}