package basico.aula_03;

public class Wrappers {
    public static void main(String[] args) {

        Integer idade = new Integer(31);
        Double precoDouble = new Double(12.45);

        Double preco = new Double("75.67");

        // conversões
        double d = preco.doubleValue();
        int i = preco.intValue();
        byte b = preco.byteValue();

        Boolean casado = new Boolean("true");

        // conversão estática
        double d1 = Double.parseDouble("125.45");
        int i1 = Integer.parseInt("123");
        float f = Float.parseFloat("3.14F");

        int i2 = Integer.valueOf("101011", 2); // 43 convertido para decimal
        System.out.println(i2);

    }
}