package basico.aula_03;

import java.util.Scanner;

public class EntradaScanner {

    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);

        System.out.println("Qual seu nome ?");
        String nome = teclado.nextLine();

        System.out.println("Seja bem-vindo " + nome);

    }
}