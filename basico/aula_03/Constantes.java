package basico.aula_03;

public class Constantes {
    public static void main(String[] args) {

        int populacao = 203429773;
        
        final double PI = 3.141592654;

        // PI = 123; 
        //erro pois PI foi marcado como final e não pode ter seu valor  alterado

        final char SEXO_MASCULINO = 'M';
        final char SEXO_FEMININO = 'F';

    }
}