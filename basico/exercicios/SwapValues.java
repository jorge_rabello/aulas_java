package basico.exercicios;

import javax.swing.JOptionPane;

public class SwapValues {
    public static void main(String[] args) {

        String x = JOptionPane.showInputDialog("Informe o valor para x: ");
        String y = JOptionPane.showInputDialog("Informe o valor para y: ");

        double xValue = Double.parseDouble(x);
        double yValue = Double.parseDouble(y);

        StringBuilder resultado1 = new StringBuilder();
        resultado1.append("Valor de x: " + xValue);
        resultado1.append("\nValor de y: " + yValue);

        JOptionPane.showMessageDialog(null, resultado1, "Resultado Antes", JOptionPane.INFORMATION_MESSAGE);

        double holder = xValue;
        xValue = yValue;
        yValue = holder;

        StringBuilder resultado2 = new StringBuilder();
        resultado2.append("Valor de x: " + xValue);
        resultado2.append("\nValor de y: " + yValue);

        JOptionPane.showMessageDialog(null, resultado2, "Resultado Depois", JOptionPane.INFORMATION_MESSAGE);

    }
}