package basico.exercicios;

import java.util.Scanner;

public class CalculoDiametro {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        
        System.out.println("Informe o valor para o raio: ");
        double raio = teclado.nextDouble();

        // Diâmetro : 2r
        double diametro = 2 * raio;

        // Circunferência  : 2PIr
        double pi = Math.PI;
        double circunferencia = 2 * pi * raio;

        // Area : PI r2
        double area = pi * (raio * raio);

        System.out.println("Diametro: " + diametro);
        System.out.println("Circunferência: " + circunferencia);
        System.out.println("Área: " + area);

    }
}