package basico.exercicios;

import java.math.BigDecimal;

import javax.swing.JOptionPane;

public class CalculoSalario {
    public static void main(String[] args) {

        final double ONE_HUNDRED = 100;

        String ht = JOptionPane.showInputDialog("Informe a quantidade de horas trabalhadas: ");
        String vh = JOptionPane.showInputDialog("Informe o valor hora: ");

        double horasTrabalhadas = Double.parseDouble(ht);
        double valorHora = Double.parseDouble(vh);

        // calcula o salário base
        double sb = horasTrabalhadas * valorHora;

        String pd = JOptionPane.showInputDialog("Informe o percentual de desconto: ");

        double percentualDesconto = Double.parseDouble(pd);

        // calcula o total de descontos
        double td = (percentualDesconto / ONE_HUNDRED) * sb;

        // calcula o salario liquido
        double sl = sb - td;

        // formata os valores para exibição
        BigDecimal salarioBase = new BigDecimal(sb).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        BigDecimal totalDescontos = new BigDecimal(td).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        BigDecimal salarioLiquido = new BigDecimal(sl).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        StringBuilder resultado = new StringBuilder();

        resultado.append("Salário Base R$ " + salarioBase);
        resultado.append("\nTotal de Descontos: R$ " + totalDescontos);
        resultado.append("\nSalário Liquído R$ " + salarioLiquido);

        JOptionPane.showMessageDialog(null, resultado, "Resultados", JOptionPane.INFORMATION_MESSAGE);
    }
}