package basico.exercicios;

import java.math.BigDecimal;

import javax.swing.JOptionPane;

public class CalculaAreaCirculo {
    public static void main(String[] args) {
        
        String r = JOptionPane.showInputDialog("Qual o raio do círculo ?");

        double raio = Double.parseDouble(r);

        double area = Math.PI * Math.pow(raio, 2);

        BigDecimal resultado = new BigDecimal(area).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        JOptionPane.showMessageDialog(null,  "Resultado:" + resultado, "Resultado", JOptionPane.INFORMATION_MESSAGE);
    }
}