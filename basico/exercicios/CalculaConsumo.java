package basico.exercicios;

import java.math.BigDecimal;

import javax.swing.JOptionPane;

public class CalculaConsumo {
    public static void main(String[] args) {

        final int TWELVE = 12;
        
        String t = JOptionPane.showInputDialog("Informe o tempo gasto na viagem: ");
        String v = JOptionPane.showInputDialog("Informe a velocidade média da viagem: ");

        double tempo = Double.parseDouble(t);
        double velocidade = Double.parseDouble(v);

        double distancia = tempo * velocidade;

        double litrosUtilizados = distancia / TWELVE;

        BigDecimal distanciaViagem = new BigDecimal(distancia).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal consumo = new BigDecimal(litrosUtilizados).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        StringBuilder resultado = new StringBuilder();
        resultado.append("Distância Percorrida: " + distanciaViagem);
        resultado.append("\nLitros Consumidos: " + consumo);

        JOptionPane.showMessageDialog(null, resultado, "Resultados", JOptionPane.INFORMATION_MESSAGE);

    }
}