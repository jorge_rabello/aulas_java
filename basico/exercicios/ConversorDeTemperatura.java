package basico.exercicios;

import java.math.BigDecimal;

import javax.swing.JOptionPane;

public class ConversorDeTemperatura {
    public static void main(String[] args) {

        String[] opcoes = { "Celsius -> Fahrenheit", "Fahrenheit -> Celsius" };

        int selecionado = JOptionPane.showOptionDialog(null, "Escolha o tipo de conversão que deseja fazer:",
                "Conversor de Temperaturas", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes,
                opcoes[0]);

        System.out.println(selecionado);

        if (selecionado == 0) {
            converteCelsiusParaFahrenheit(JOptionPane.showInputDialog("Informe a temperatura em Graus Celsius: "));
        } else {
            converteFahrenheitParaCelsius(JOptionPane.showInputDialog("Informe a temperatura em Graus Fahrenheit: "));
        }
    }

    private static void converteFahrenheitParaCelsius(String f) {

        final int NINE = 9;
        final int FIVE = 5;
        final int THIRTY_TWO = 32;

        double fahrenheit = Double.parseDouble(f);

        double c = ((fahrenheit - THIRTY_TWO) * FIVE) / NINE;

        BigDecimal celsius = new BigDecimal(c).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        StringBuilder resultado = new StringBuilder();

        resultado.append("Temperatura em Fahrenheit: " + fahrenheit);
        resultado.append("\nTemperatura em Celsius: " + celsius);
        
        JOptionPane.showMessageDialog(null, resultado, "Resultado", JOptionPane.INFORMATION_MESSAGE);

    }

    private static void converteCelsiusParaFahrenheit(String c) {

        final int NINE = 9;
        final int FIVE = 5;

        double celsius = Double.parseDouble(c);
        double f = (NINE * celsius) / FIVE;

        BigDecimal fahrenheit = new BigDecimal(f).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        StringBuilder resultado = new StringBuilder();

        resultado.append("Temperatura em Celsius: " + celsius);
        resultado.append("\nTemperatura em Fahrenheit: " + fahrenheit);

        JOptionPane.showMessageDialog(null, resultado, "Resultado", JOptionPane.INFORMATION_MESSAGE);
    }
}