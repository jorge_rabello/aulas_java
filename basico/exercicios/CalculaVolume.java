package basico.exercicios;

import java.math.BigDecimal;

import javax.swing.JOptionPane;

public class CalculaVolume {
    public static void main(String[] args) {

        String r = JOptionPane.showInputDialog("Informe o raio da circunferência da lata: ");
        String a = JOptionPane.showInputDialog("Informe a altura da lata: ");

        double raio = Double.parseDouble(r);
        double altura = Double.parseDouble(a);

        double v = Math.PI * Math.pow(raio, 2) * altura;

        BigDecimal volume = new BigDecimal(v).setScale(2, BigDecimal.ROUND_HALF_EVEN);

        StringBuilder resultado = new StringBuilder();

        resultado.append("Raio: " + raio);
        resultado.append("\nAltura: " + altura);
        resultado.append("\nVolume: " + volume + " cm3");

        JOptionPane.showMessageDialog(null, resultado, "Resultado", JOptionPane.INFORMATION_MESSAGE);
    }
}