package basico.aula_02;

public class Variaveis {

    public static void main(String[] args) {

        System.out.println("Jorge Rabello");

        // declaração de variáveis
        String nome = "Jorge Rabello"; // variável de texto
        int idade = 31; // variável de numero inteiro
        boolean casado = true; // ou false

        System.out.println(nome);
        System.out.println(casado);
        System.out.println(idade);
        System.out.println(nome);

        String firstName, lastName;

        firstName = "Jorge";
        lastName = "Rabello";

        System.out.println("\nNome: " + firstName + " " + "\nSobrenome: " + lastName);
    }

}