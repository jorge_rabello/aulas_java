package basico.aula_02;

public class TiposDeDados {

    public static void main(String[] args) {

        // tipos
        int idade = 31; // numérico inteiro
        double preco = 12.45; // numérico decimal
        char sexo = 'M'; // caractere
        boolean casado = true; // booleano (verdadeiro ou falso)

        System.out.println(idade);
        System.out.println(preco);
        System.out.println(sexo);
        System.out.println(casado);

        short s = 32_767;
        int i = 2_000_000_000;
        long l = 9_000_000L;

        // cast implícito
        i = s;

        System.out.println(i);
        System.out.println(s);

        // cast explícito
        i = (int) l;
        System.out.println(i);
        System.out.println(l);

    }
}