package basico.aula_04;

public class OperadoresDeComparacao {
    public static void main(String[] args) {

        int x = 6;

        System.out.println(x == 6); // true
        System.out.println(x != 6); // false
        System.out.println(x > 6); // false
        System.out.println(x < 6); // false
        System.out.println(x >= 6); // true
        System.out.println(x <= 6); // true
        System.out.println("OI" instanceof String); // true

    }
}