package basico.aula_04;

public class OperadorLogicoNao {
    public static void main(String[] args) {
        
        int x = 6;

        // OPERADOR NÃO
        System.out.println(!(x < 1)); // !falso = verdaeiro
        System.out.println(!(x < 12)); // !verdadeiro = falso

    }
}