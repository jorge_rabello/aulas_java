package basico.aula_04;

public class OperadoresAritmeticos {
    public static void main(String[] args) {

        double x = 7 + 3;
        double y = +9;
        System.out.println(x);
        System.out.println(y);

        String oi = "Oi ";
        String profissao = "Programador Java";
        String frase = oi + profissao;
        System.out.println(frase);

        double x1 = 10 % 2;
        double y1 = 5 % 2;
        System.out.println(x1);
        System.out.println(y1);

        double x2 = 7 - 3;
        double y2 = -9;
        System.out.println(x2);
        System.out.println(y2);

        double x3 = 7 * 3;
        System.out.println(x3);

        double x4 = 10 / 2;
        System.out.println(x4);

        double x5 = -(+3);
        double y5 = -(-3);
        System.out.println(x5);
        System.out.println(y5);

        double x6 = 15;
        double y6 = 15;

        x6++;
        y6--;

        System.out.println(x6);
        System.out.println(y6);

        double x7 = 15;
        double y7 = x++; // pós incremento

        System.out.println("X: " + x7);     // 16
        System.out.println("Y: " + y7);    // 15

        double x8 = 15;
        double y8 = ++x; // pré incremento

        System.out.println("X: " + x8);     // 16
        System.out.println("Y: " + y8);    // 16


    }
}