package basico.aula_04;

public class OperadorLogicoE {
    public static void main(String[] args) {

        int x = 6;
        boolean resultado;

        // OPERADOR E

        // falso e falso = falso
        resultado = (x > 12) && (x < 1);
        System.out.println(resultado);

        // falso e verdadeiro = falso
        resultado = (x > 12) && (x < 12);
        System.out.println(resultado);

        // verdadeiro e falso = falso
        resultado = (x > 1) && (x > 12);
        System.out.println(resultado);

        // verdadeiro e verdadeiro = verdadeiro
        resultado = (x > 1) && (x < 12);
        System.out.println(resultado);
    }

}