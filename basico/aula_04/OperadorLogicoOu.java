package basico.aula_04;

public class OperadorLogicoOu {
    public static void main(String[] args) {

        int x = 6;
        boolean resultado;

        // OPERADOR OU

        // falso ou falso = falso
        resultado = (x > 12) || (x < 1);
        System.out.println(resultado);

        // falso ou verdadeiro = verdadeiro
        resultado = (x > 12) || (x < 12);
        System.out.println(resultado);

        // verdadeiro ou falso = verdadeiro
        resultado = (x > 1) || (x > 12);
        System.out.println(resultado);

        // verdadeiro e verdadeiro = verdadeiro
        resultado = (x > 1) || (x < 12);
        System.out.println(resultado);

    }
}