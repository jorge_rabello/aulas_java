package basico.aula_04;

public class OperadoresEspeciais {
    public static void main(String[] args) {

        int idade = 26;

        String x = (idade >= 18) ? "Maior de Idade" : "Menor de Idade";

        System.out.println(x);

    }
}